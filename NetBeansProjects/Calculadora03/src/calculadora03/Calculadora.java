package calculadora03;

import javax.swing.JComboBox;

public class Calculadora {
    public float N1;
    public float N2;
   
    private CalculadoraFRM other;
    public Calculadora() {
    }
    public Calculadora(float N1, float N2) {
        this.N1 = N1;
        this.N2 = N2;
   
    }

    public float getN1() {
        return N1;
    }

    public void setN1(float N1) {
        this.N1 = N1;
    }

    public float getN2() {
        return N2;
    }

    public void setN2(float N2) {
        this.N2 = N2;
    }
       
    public float Suma()
    {
      return N1+N2;
    }
    public float Resta()
    {
      return N1-N2;
    }
    public float Multi()
    {
      return N1*N2;
    }
    public float Division()
    {
       return N1/N2;
    }
    
    
    
}
