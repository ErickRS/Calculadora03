package calculadora03;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener{
    private CalculadoraFRM vista;
    private Calculadora modelo;

    public Controlador(CalculadoraFRM vista, Calculadora modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    public void iniciar(){
    vista.setTitle("Calculadora");//Titulo de la ventana
    vista.setLocationRelativeTo(null);//Ubicar centro de la pantalla
    vista.setVisible(true);// mostrar
    asignarControl();
    }
    private void asignarControl(){
    
    vista.getCalcular().addActionListener(this);
    vista.getLimpiar().addActionListener(this);
    vista.getCerrar().addActionListener(this);
    }
    private void limpiar(){
    
        //enlaar con las cajas de texto del formulario vista
        vista.getN1().setText("");
        vista.getN2().setText("");
        
        //Limpiar la etiqueta area
        vista.getResultado().setText("");
    }
    //Metodo para cerrar
    private void cerrar(){
    System.exit(0);
    }
    
    public void calcular(){
        float N1 = Float.parseFloat(vista.getN1().getText());
        float N2 = Float.parseFloat(vista.getN2().getText());
        
        Object operador = vista.getOperaciones().getSelectedItem();
        // se envian datos a modelo
        modelo.setN1(N1);
        modelo.setN2(N2);
        
        if(operador.equals("+")){
        vista.getResultado().setText("Resultado "+ modelo.Suma());}
        if(operador.equals("-")){
        vista.getResultado().setText("Resultado "+ modelo.Resta());}
        
        if(operador.equals("*")){
        vista.getResultado().setText("Resultado "+ modelo.Multi());}
        if(operador.equals("/")){
        vista.getResultado().setText("Resultado "+ modelo.Division());}
        }
        
    
    
    

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource()== vista.getCalcular())
            calcular();
        if(ae.getSource() == vista.getLimpiar())
            limpiar();
        if(ae.getSource() == vista.getCerrar())
            cerrar();
    }
    
}
