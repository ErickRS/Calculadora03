/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora03;

import java.awt.EventQueue;

/**
 *
 * @author Sk0ry
 */
public class Calculadora03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {

           @Override
           public void run() {
       
               //lanzar el controlador
               new Controlador(new CalculadoraFRM(), new Calculadora()).iniciar();
               
               
               
           }
       });
    }
    
}
